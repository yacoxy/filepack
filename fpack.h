#ifndef FPACK_H
#define FPACK_H

#include "global.h"

/**
 * @brief 文件目录结构
 */
class Element{
public:
    explicit Element()
        : isFile(false)
        , m_hashNum(0)
        , m_offset(0)
        , m_length(0)
    {}

    ~Element()
    {
        while(!m_chirldList.isEmpty()){
            Element *temp = m_chirldList.takeFirst();
            if(temp != nullptr){
                delete temp;
            }
        }
    }

    QString toString() const;

public:
    QString m_name;                 //文件或者文件夹的名字
    bool isFile;                    //是否为文件

    ulong m_hashNum;                //为文件时，保存其计算出来的哈希数字
    QString m_fullPath;             //为文件时，保存其全路径名称，用于后续打包新文件
    qint64 m_offset;
    qint64 m_length;

    QList<Element*> m_chirldList;    //如果为文件夹，存储其子目录及子文件

private:
    static uint m_level;
};

class FPack : public QObject
{
    Q_OBJECT
public:
    explicit FPack()
        : m_rv(RV_1)
    {}

    void startPack(QStringList);

private:
    void setHashNum(Element *fNode,QString prefix = QString());
    bool pack();

public:
    void setDstPath(QString);
    void setVersion(ResourceVersion rv);

    static void traverseDir(Element *fNode,QString dirPath,uint &fileCount);

signals:

public slots:

private:
    Element *m_head;                //头节点指针，指向最上层文件目录
    uint m_hashLength;              //根据长度文件数量计算出来的哈希表长度

    ResourceVersion m_rv;           //资源所用版本

private:
    QList<Element*> m_fileList;     //所有的文件的Element指针列表

    QString m_dstPath;              //新文件的目标全路径
};

#endif // FPACK_H
