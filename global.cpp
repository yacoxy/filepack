#include "global.h"
#include <QDebug>

/**
 * @brief 根据元素数量和哈希表装填因子计算合适的哈希表长度，往上取最接近eleCount / hashParam的素数
 * @param eleCount : 元素数量
 * @param hashParam : 哈希表装填因子
 * @return : 返回计算出来的哈希表长度
 */
uint getProperHashLength(int eleCount, double hashParam)
{
    int hashLength = eleCount / hashParam;
    for(int i=0;;i++){
        if(isPrime(hashLength + i)){
            return hashLength + i;
        }
    }
}

/**
 * @brief : 判断一个数字是否为素数
 * @param num : 要判断的数字
 * @return : 是否为素数
 */
bool isPrime(uint num)
{
    if(num <= 1){
        return false;
    }
    if(num == 2){
        return true;
    }
    if(num % 2 == 0){
        return false;
    }

    int mid = (int)sqrt((double)num) + 1;
    for(int i=3;i<mid;i+=2){
        if(num % i == 0){
            return false;
        }
    }
    return true;
}

/**
 * @brief 使用BKDRHash算法获取字符串对应的哈希值
 * @param str : 要求值的字符串
 * @return  : 对应的哈希值
 */
ulong getHashNum(QString str)
{
    //BKDRHash
    ulong hash = 0;
    for(int i=0;i<str.length();++i){
        hash = hash * 131 + (ulong)str[i].digitValue();   // 也可以乘以31、131、1313、13131、131313..
    }
    return hash;
}
