#include "fseparate.h"

#include <string.h>

FSeparate::FSeparate(QString dstFilePath,QObject *parent)
    : QObject(parent)
    , m_head(nullptr)
{
    setDstFilePath(dstFilePath);
}

FSeparate::~FSeparate()
{
    if(m_head != nullptr){
        delete m_head;
        m_head = nullptr;
    }

    while(!m_fileList.isEmpty()){
        FileMes *temp = m_fileList.takeFirst();
        if(temp != nullptr){
            delete temp;
        }
    }
}

/**
 * @brief 设置要解析的目标文件，需为新的文件类型，暂时未考虑验证的问题；
 *        考虑能够改变目标文件，所以有返回值，确认是否更换成功
 * @param dstFilePath : 目标文件路径
 * @return 设置是否成功
 */
bool FSeparate::setDstFilePath(QString dstFilePath)
{
    if(dstFilePath.isEmpty()){
        return false;
    }

    m_dstFile.setFileName(dstFilePath);
    if(!m_dstFile.exists() || !m_dstFile.open(QIODevice::ReadOnly)){
        return false;
    }

    //为有效路径时，先清除之前的信息
    m_dstFilePath = dstFilePath;
    if(m_head != nullptr){
        delete m_head;
    }
    while(!m_fileList.isEmpty()){
        FileMes *temp = m_fileList.takeFirst();
        if(temp != nullptr){
            delete temp;
        }
    }

    m_head = new ContentNode();

    //解析打包时存放的字符串，获得需要的版本信息、结构信息以及文件信息，分别通过m_head和m_fileList索引
    QString contentStr(m_dstFile.readLine());
    QStringList strList = contentStr.split(VersionSeparator,QString::SkipEmptyParts);
    if(strList.length() != 2){
        return false;
    }
    m_rv = (ResourceVersion)strList[0].toInt();
    contentStr = strList[1];
    analyseStructure(m_head,contentStr.remove('\n'),"",0);

    //计算哈希表长度
    m_hashLength = getProperHashLength(m_fileList.length(),HashParam);

    //建立散列表，根据散列值将m_fileList中的数据重新排列
    QList<FileMes *> tempList = m_fileList;
    m_fileList.clear();
    for(int i=0;i<tempList.length();i++){
        uint hashNum = tempList[i]->m_hashNum;
        while(m_fileList.length() <= hashNum){
            m_fileList.append(nullptr);
        }
        if(m_fileList[hashNum] == nullptr){
            m_fileList[hashNum] = tempList[i];
        }else{
            FileMes *node = m_fileList[hashNum];
            while(node->next != nullptr){
                node = node->next;
            }
            node->next = tempList[i];
        }
    }

    return true;
}

/**
 * @brief 设置资源包版本
 * @param rv : 版本值
 */
void FSeparate::setVersion(ResourceVersion rv)
{
    m_rv = rv;
}

/**
 * @brief 获取解析出来的目录结构头节点
 * @return  解析出来的目录结构头节点
 */
ContentNode *FSeparate::getContentStructure()
{
    return m_head;
}

/**
 * @brief 通过文件名获取文件内容及长度
 * @param_IN fileName : 需要获取的文件名，为相对目录结构的相对路径
 * @param_OUT startPtr : 文件内容起始指针
 * @param_IN&OUT length : 文件长度
 * @return : 操作是否成功
 */
bool FSeparate::getFileContent(QString fileName, uchar *startPtr, ulong &length)
{
    //根据传入文件名计算对应哈希值
    ulong hashNum = getHashNum(fileName) % m_hashLength;
    if(hashNum >= m_fileList.length()){
        return false;
    }

    //在已经建立的哈希表中查找对应的节点
    bool findResult = false;
    FileMes *node = m_fileList.at(hashNum);
    while(node != nullptr){
        if(node->m_name == fileName){
            findResult = true;
            break;
        }
        node = node->next;
    }
    if(!findResult){
        //qDebug() << fileName << " not found";
        return false;
    }

    //将结果赋给输出参数
    if(length == 0){
        length = node->m_length;
    }

    uchar *ptr = m_dstFile.map(node->m_offset - 1,length);
    startPtr = new uchar[length+1];
    memcpy(startPtr,ptr,length);
    m_dstFile.unmap(ptr);

//    //测试部分，将得到的内容重新建成新的文件放入对应目录下
//    QString name = QString("/Users/kangchaoya/Project/new").append(fileName);
//    QFile file(name);
//    if(!file.open(QIODevice::WriteOnly)){
//        //qDebug() << "can't create file.";
//        return false;
//    }
//    file.write((char *)startPtr,length);
//    file.close();

//    m_dstFile.unmap(startPtr);

    return true;
}

/**
 * @brief 多线程测试函数
 */
void FSeparate::test()
{
    QStringList nameList;
    for(int i=0;i<m_fileList.length();i++){
        FileMes *node = m_fileList.at(i);
        while(node != nullptr){
            nameList.append(node->m_name);
            node = node->next;
        }
    }

    int midLength = nameList.length() / 3;
    QStringList list1,list2;
    for(int i=0;i<midLength;i++){
        list1.append(nameList.takeFirst());
    }
    for(int i=0;i<midLength;i++){
        list2.append(nameList.takeFirst());
    }

    ReadFileThread *thread1 = new ReadFileThread(this,list1);
    thread1->start();

    ReadFileThread *thread2 = new ReadFileThread(this,list2);
    thread2->start();

    ReadFileThread *thread3 = new ReadFileThread(this,nameList);
    thread3->start();
}

/**
 * @brief 以递归的方式解析目录结构字符串
 * @param fNode : 存放当前目录结构的头节点
 * @param contentStr : 要解析的目录信息字符串
 * @param prefix : 当前目录下的前缀
 * @param level : 目录中的层级
 */
void FSeparate::analyseStructure(ContentNode * fNode,QString contentStr,QString prefix,int level)
{
    if(contentStr.isEmpty() || fNode == nullptr){
        return;
    }

    //每进入一层文件夹先将level计数＋1
    level++;

    //先根据冒号位置确定名字
    uint colonPos = 0;
    for(int i=0;i<contentStr.length();++i){
        if(contentStr[i] != QChar(':')){
            continue;
        }
        colonPos = i;
        break;
    }
    fNode->m_name = contentStr.mid(0,colonPos);

    //是文件时将对应信息存入m_fileHList
    if(colonPos + 1 < contentStr.length() && contentStr[colonPos+1] == QChar('(')){
        QStringList numStrList = contentStr.mid(colonPos + 2,contentStr.length() - colonPos - 3).split(',',QString::SkipEmptyParts);
        if(numStrList.length() == 3){
            FileMes *filemes = new FileMes();
            filemes->m_name = QString(prefix).append('/').append(fNode->m_name);
            while(filemes->m_name.contains("//")){
                filemes->m_name.replace("//","/");
            }
            filemes->m_hashNum = numStrList.at(0).toUInt();
            filemes->m_offset = numStrList.at(1).toULongLong();
            filemes->m_length = numStrList.at(2).toULongLong();
            m_fileList.append(filemes);
        }

        return;
    }

    //然后解析子节点字符串
    QStringList chirldStrList = contentStr.mid(colonPos + 2,contentStr.length() - colonPos - 3).split(Separator(level),QString::SkipEmptyParts);
    for(int i=0;i<chirldStrList.length();i++){
        ContentNode *node = new ContentNode();
        fNode->m_chirldList.append(node);
        analyseStructure(node,chirldStrList[i],QString(prefix).append('/').append(fNode->m_name),level);
    }
}

void ReadFileThread::run()
{
    qDebug() << "start run : ";
    for(int i=0;i<m_fileNameList.length();i++){
        uchar *ptr = nullptr;
        ulong length = 0;
        QString m_fileName = m_fileNameList[i];
        bool result = m_fSeparate->getFileContent(m_fileName,ptr,length);
        if(!result){
            continue;
        }

//        QString name = QString("/Users/kangchaoya/Project/new").append(m_fileName);
//        qDebug() << "name : " << name;
//        QFile file(name);
//        if(!file.open(QIODevice::WriteOnly)){
//            return;
//        }
//        file.write((char *)ptr,length);
//        file.close();
    }
}
