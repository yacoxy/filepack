#ifndef GLOBAL_H
#define GLOBAL_H

#include <math.h>
#include <ctype.h>

#include <QObject>
#include <QString>
#include <QStringList>
#include <QPair>
#include <QDebug>

#include <QFileInfo>
#include <QFile>
#include <QDataStream>
#include <QDir>

#include <QThread>

#define Separator(i) QString("{[(%1)]}").arg(i)
#define HashParam 0.5
#define VersionSeparator QString("###")
const int MaxMapSize = 10 * 1024 * 1024;

/**
 * @brief 资源包版本枚举，后续版本更新时以此作标识
 */
enum ResourceVersion{
    RV_1 = 1
};

//计算哈希表长度
uint getProperHashLength(int listLength,double hashParam);

//素数判断
bool isPrime(uint num);

//计算字符串对应的哈希数字
ulong getHashNum(QString str);

#endif // GLOBAL_H
