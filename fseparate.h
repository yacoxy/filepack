#ifndef FSEPARATE_H
#define FSEPARATE_H

#include <QObject>

#include "global.h"

/**
 * @brief 文件目录单个节点类
 */
class ContentNode
{
public:
    ContentNode(){}
    ~ContentNode()
    {
        while(!m_chirldList.isEmpty()){
            ContentNode *temp = m_chirldList.takeFirst();
            if(temp != nullptr){
                delete temp;
            }
        }
    }

public:
    QString m_name;                     //当前节点的文件/文件夹名称
    QList<ContentNode *> m_chirldList;  //子节点列表
};

/**
 * @brief 文件哈希表中的单个节点，包含文件的基本信息如下属性所示
 */
class FileMes
{
public:
    FileMes()
        : m_hashNum(0)
        , m_offset(0)
        , m_length(0)
        , next(nullptr)
    {}

    ~FileMes()
    {
        if(next != nullptr){
            delete next;
        }
    }

public:
    QString m_name;     //文件在整个结构目录中的名称
    uint m_hashNum;     //文件名计算出来的哈希数
    qint64 m_offset;    //文件起始位置偏移
    qint64 m_length;    //文件内容长度

    FileMes *next;      //在哈希表中如果有相同的哈希数值则以链表的方式往后放
};

/**
 * @brief 新文件结构解析及内容获取的类
 */
class FSeparate : public QObject
{
    Q_OBJECT
public:
    explicit FSeparate(QString dstFilePath,QObject *parent = nullptr);
    ~FSeparate();

    bool setDstFilePath(QString dstFilePath);
    void setVersion(ResourceVersion rv);

    ContentNode * getContentStructure();
    bool getFileContent(QString fileName,uchar *startPtr,ulong &length=0);

    void test();

private:
    void analyseStructure(ContentNode *,QString,QString,int level);

signals:

public slots:

private:
    ContentNode *m_head;            //存放新文件中对应目录结构的头节点
    QList<FileMes *> m_fileList;    //以哈希表的方式存放所有原文件

    QString m_dstFilePath;          //要解析的文件所在路径
    ResourceVersion m_rv;           //资源包版本，暂时没用，后续可以根据版本信息实现不同的文件头解析函数

    QFile m_dstFile;                //要解析的文件

    ulong m_hashLength;             //根据长度文件数量计算出来的哈希表长度
};

/**
 * @brief 多线程还原测试类
 */
class ReadFileThread : public QThread
{
public:
    ReadFileThread(FSeparate* fSeparate,QStringList fileNameList)
        : m_fSeparate(fSeparate)
        , m_fileNameList(fileNameList)
    {}

    void run();
private:
    FSeparate *m_fSeparate;
    QStringList m_fileNameList;
};

#endif // FSEPARATE_H
