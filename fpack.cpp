#include "fpack.h"
#include "global.h"

uint Element::m_level = 0;

/**
 * @brief 将文件目录转换成固定结构的字符串；文件夹格式为m_name:{内部内容}，文件结构为m_name:(m_hashNum,m_offset,m_length);
 * @return : 将最终结构返回
 */
QString Element::toString() const
{
    m_level++;
    QString resultStr(m_name);
    if(isFile){
        resultStr.append(":(")
                .append(QString("%1").arg(m_hashNum,10,10,QChar('0')))
                .append(',')
                .append(QString("%1").arg(m_offset,20,10,QChar('0')))
                .append(',')
                .append(QString("%1").arg(m_length,20,10,QChar('0')))
                .append(")");
        m_level--;
        return resultStr;
    }
    resultStr.append(":{");
    for(int i=0;i<m_chirldList.length();++i){
        resultStr.append(m_chirldList[i]->toString());
        resultStr.append(Separator(m_level));
    }
    resultStr.append("}");
    m_level--;
    return resultStr;
}

/**
 * @brief : 开始打包函数，将需要打包的所有文件的全路径做参数，生成
 *              1.文件目录结构；
 *              2.散列表，包含散列位置信息及文件在新文件中的位置
 *              3.新文件
 * @param fileList : 需要打包的文件的全路径
 */
void FPack::startPack(QStringList fileList)
{
    //文件计数，用以后面计算哈希表长度
    uint fileCount = 0;

    //构建头节点
    m_head = new Element();
    m_head->m_name = "/";

    //将所有要处理的文件或目录构建成文件目录结构
    for(int i=0;i<fileList.length();++i){
        QFileInfo fileInfo(fileList[i]);
        if(!fileInfo.exists()){
            continue;
        }
        Element *node = new Element();
        node->m_name = fileInfo.fileName();
        if(fileInfo.isFile()){
            //文件计数＋1
            ++fileCount;

            node->isFile = true;
            node->m_fullPath = fileInfo.absoluteFilePath();
        }else if(fileInfo.isDir()){
            traverseDir(node,fileList[i],fileCount);
        }
        m_head->m_chirldList.append(node);
    }

//    qDebug() << "fileCount : " << fileCount;

    //计算合适的哈希表长度
    m_hashLength = getProperHashLength(fileCount,HashParam);

//    qDebug() << "m_hashLength : " << m_hashLength;

    //处理文件名，通过文件名得到其在哈希散列表中的位置
    setHashNum(m_head);

    //将文件打包成新文件
    pack();
}

/**
 * @brief 递归遍历整个树，将其中文件节点在哈希表中的数字计算出来记录下来
 * @param fNode : 父节点指针
 * @param prefix : 当前递归前缀字符串，用于记录相对位置
 */
void FPack::setHashNum(Element *fNode, QString prefix)
{
    if(fNode == nullptr){
        return;
    }
    for(int i=0;i<fNode->m_chirldList.length();++i){
        Element *node = fNode->m_chirldList[i];
        if(!node->isFile){
            setHashNum(node,QString(prefix).append("/").append(node->m_name));
            continue;
        }
        node->m_hashNum = getHashNum(QString(prefix).append("/").append(node->m_name)) % m_hashLength;
//        qDebug() << "pack hash str : " << QString(prefix).append("/").append(node->m_name) << "   hashNum : " << node->m_hashNum;
        m_fileList.append(node);
    }
}

/**
 * @brief 打包函数，暂时不考虑文件压缩，只简单的将文件拷贝到新文件中，然后将新文件放入目标文件夹中即可。
 *        将整个目录结构信息放在新文件开头部分，后接文件内容，需记录文件起始偏移以及文件长度，以便从中取出内容。
 * @return : 打包是否成功
 */
bool FPack::pack()
{
    //先确定起始目录结构字符串的长度，因为需要将其放在文件起始位置
    qint64 headLength = m_head->toString().length() + 2;
    qDebug() << "headLength : " << headLength;

    //以headLength为起始偏移，得出每一个文件在新文件中的偏移以及大小
    for(int i=0;i<m_fileList.length();++i){
        Element *node = m_fileList[i];
        QFile tempFile(node->m_fullPath);
        if(!tempFile.exists() || !tempFile.open(QIODevice::ReadOnly)){
            m_fileList.removeAt(i--);
            continue;
        }
        node->m_offset = headLength;
        node->m_length = tempFile.size();
        headLength += node->m_length;
        tempFile.close();
    }
//    qDebug() << "Strucure : " << m_head->toString();

    //在目标路径下创建新文件
    if(m_dstPath.isEmpty()){
        return false;
    }
    QFile dstFile(m_dstPath);
    if(!dstFile.open(QIODevice::WriteOnly)){
        return false;
    }

    //首先将目录结构字符串存入新文件
    QString str = m_head->toString();
    str.prepend(QString("%1%2%3").arg(VersionSeparator).arg(m_rv).arg(VersionSeparator));
    dstFile.write(str.append('\n').toLocal8Bit());

    //再将文件逐一写入新文件
    for(int i=0;i<m_fileList.length();i++){
        Element *node = m_fileList[i];
        QFile tempFile(node->m_fullPath);
        if(!tempFile.exists() || !tempFile.open(QIODevice::ReadOnly)){
            m_fileList.removeAt(i--);
            continue;
        }
        qint64 fileSize = tempFile.size();
        qint64 startPos = 0;
        while(startPos < fileSize){
            qint64 length = fileSize > (startPos + MaxMapSize) ? MaxMapSize : (fileSize - startPos);
            uchar *fileStart = tempFile.map(startPos,length);
            dstFile.write((char *)fileStart,length);
            startPos += length;
            tempFile.unmap(fileStart);
        }
    }

    dstFile.close();
    return true;
}

/**
 * @brief 设置生成的新文件的全路径
 * @param 生成的新文件的全路径
 */
void FPack::setDstPath(QString dstPath)
{
    m_dstPath = dstPath;
}

/**
 * @brief 设置当前资源包版本
 * @param 版本数据
 */
void FPack::setVersion(ResourceVersion rv)
{
    m_rv = rv;
}

/**
 * @brief 递归遍历文件夹以构建新文件结构
 * @param fNode : 要遍历的文件夹所在的节点引用
 * @param dirPath : 要遍历的文件夹全路径
 */
void FPack::traverseDir(Element *fNode,QString dirPath,uint &fileCount)
{
    QDir dir(dirPath);
    if(!dir.exists()){
        return;
    }
    QFileInfoList fileInfoList = dir.entryInfoList();
    for(int i=0;i<fileInfoList.length();++i){
        QString filename = fileInfoList[i].fileName();
        //文件目录中会有“.”和“..”两个无需处理的目录
        if(filename == "." || filename == ".."){
            continue;
        }

        Element *node = new Element();
        node->m_name = filename;
        if(fileInfoList[i].isFile()){
            ++fileCount;
            node->isFile = true;
            node->m_fullPath = fileInfoList[i].absoluteFilePath();
        }else if(fileInfoList[i].isDir()){
            traverseDir(node,fileInfoList[i].absoluteFilePath(),fileCount);
        }
        fNode->m_chirldList.append(node);
    }
}
